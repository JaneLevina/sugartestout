<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\Message;

class MessageController extends Controller
{

  public function store(Request $request)
  {
  	$rules = [
  		'name' 		=> 'required',
  		'message' 	=> 'required',
  		'email' 	=> 'required|email',
  	];
  	$validator = Validator::make($request->all(), $rules);
  	
  	if ($validator->fails()) {
  		return json_encode(['error' => true, 'message' => $validator->messages()]);
  	}

  	$message = new Message;

  	$message->name = $request->input('name');
  	$message->message = $request->input('message');
  	$message->email = $request->input('email');
  	$message->date_created = date('Y-m-d H:i:s');
  	$message->sent = false;
  	
  	if ($message->save()) {
  		return json_encode(['error' => false]);
  	}

  	return json_encode(['error' => true, 'message' => 'Message do not saved.']);
  }
}