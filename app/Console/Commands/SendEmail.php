<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Message;
use Mail;

class SendEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        

        $messages = Message::where('sent', false)
            ->where('date_created', '<', date('Y-m-d H:i:s', time() - config('app.send_email_delay')))
            ->get();

        foreach ($messages as $row) {
            preg_match('/@([\w.]+)/', $row->email, $matches);
            $domen = $matches[1];

            if (in_array($domen, ['mail.ru', 'rambler.ru', 'yandex.ru'])) {
                $template = 'emails.template_1';
                $subject = 'К сожалению, мы не можем вам помочь';
            } else {
                $template = 'emails.template_2';
                $subject = 'Всё отлично!';
            }

            $mail = Mail::send(
                $template, 
                ['data' => $row, 'domen' => $domen], 
                function($message) use ($row, $subject) {
                    $message->to($row->email, $row->name)->subject($subject);
            });
            
            if (!Mail::failures()) {
                $row->sent = true;
                $row->save();
            }
        }

    }
}
