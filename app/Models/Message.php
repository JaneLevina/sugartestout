<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class Message extends Model
{
	use Uuids;

    protected $table = 'messages';
    protected $fillable = ['name', 'message', 'email', 'date_created', 'sent'];

    public $timestamps = false;
    public $incrementing = false;
}
